package com.example.kenneth.assignment1;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;




public class MyDBHandler extends SQLiteOpenHelper {

    /*Set the names of the database and its table and columns. also sets the version to 2
    after I made some changes to the database structure  */
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "locationsDB.db";
    public static final String TABLE_LOCATIONS = "locations";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_TIME = "time";

    //We need to pass database information along to superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    //Create the table Locations where we store the time and city
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_LOCATIONS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CITY + " TEXT, " +
                COLUMN_TIME + " TEXT " +
                ");";
        db.execSQL(query);
    }

    //Drops the previous database when upgraded
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATIONS);
        onCreate(db);
    }

    //Add a new row to the database
    public void addLocation(String city, String time){
        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY, city);
        values.put(COLUMN_TIME, time);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_LOCATIONS, null, values);
        db.close();
    }
    //Empty the database
    public void clearDatabase(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_LOCATIONS, null, null);
        db.close();
    }

    /* Getting the time and city from the database, and output it as a string
    ordering by id, descending so that the latest location comes first,
    and only show the 5 most recent locations */
    public String databaseToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_LOCATIONS + " WHERE 1 ORDER BY " + COLUMN_ID +
                " DESC LIMIT 5;";

        //Initialize the cursor and move to the first row
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        //Loop through the results and add it all to a string, ending with a linebreak
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex("city")) != null) {
                dbString += "You were in ";
                dbString +=  c.getString(c.getColumnIndex("city"));
                dbString += " at " + c.getString(c.getColumnIndex("time"));
                dbString += "\n";
            }
            c.moveToNext();
        }
        //Close the database and return it as a string
        db.close();
        return dbString;
    }

}