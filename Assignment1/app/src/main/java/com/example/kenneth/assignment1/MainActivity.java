package com.example.kenneth.assignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    MyDBHandler dbHandler;
    TextView myText;
    Button deleteButton;
    Button refreshButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get Ids for buttons and text area
        myText = (TextView)findViewById(R.id.myText);
        deleteButton = (Button)findViewById(R.id.deleteButton);
        refreshButton = (Button)findViewById(R.id.refreshButton);

        //gets the database as a string and outputs it in myText
        dbHandler = new MyDBHandler(this,null,null,1);
            String dbString = dbHandler.databaseToString();
            myText.setText(dbString);

        // when delete button is clicked, clear the database, and print the empty string
        deleteButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        clearDatabase();
                        String dbString = dbHandler.databaseToString();
                        myText.setText(dbString);
                    }
                }
        );

        //When the refresh button is clicked, print the database
        refreshButton.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        String dbString = dbHandler.databaseToString();
                        myText.setText(dbString);
                    }
                }
        );
    }
    // Sends the user to the map location when button1 is clicked
    public void goToMap(View view)
    {
        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
        startActivity(intent);
    }
    //Clearing the database
    protected void clearDatabase(){
        dbHandler.clearDatabase();

    }
}
