package com.example.kenneth.assignment1;

public class Location {

    private int _id;
    private String _city;
    private String _time;

    public Location(){
    }

    public Location(String city, String time){
        this._city = city;
        this._time = time;
    }

    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_city(String _city) {
        this._city = _city;
    }

    public int get_id() {
        return _id;
    }

    public String get_city() {
        return _city;
    }

}